package combatlogparser.events.interfaces;

public interface EventInterface {
	public int parse(String timeDate, String[] values);
}